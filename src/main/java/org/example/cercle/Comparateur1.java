package org.example.cercle;

import java.util.Comparator;

public class Comparateur1 implements Comparator<Cercle> {

    public int compare(Cercle c1, Cercle c2) {
        double r1 = c1.getRayon();
        double r2 = c2.getRayon();
        if (r1 < r2) return -1;
        else if (r1 == r2) return 0;
        else return 1;

    }

}
