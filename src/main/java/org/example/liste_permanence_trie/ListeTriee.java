package org.example.liste_permanence_trie;

import java.util.LinkedList;
import java.util.ListIterator;

public class ListeTriee {

    public ListeTriee() {
        liste = new LinkedList<String>();        //	ou	ArrayList
    }

    public void ajoute(String ch) {
        ListIterator<String> it = liste.listIterator();
        boolean trouve = false;
        while ((it.hasNext()) && !trouve) {
            if (it.next().compareTo(ch) > 0) trouve = true;
        }
        if (trouve) it.previous();     //	 ici,	 il	 y	 obligatoirement un precedent
        it.add(ch);
    }

    public void affiche() {
        for (String ch : liste) System.out.print(ch + "	");
        System.out.println();
    }

    private LinkedList<String> liste;                //	ou	ArrayList
}

